<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Model\user\post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostContraller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function post()
    {
        $category=Category::all();

        return view('posts.post',['category'=>$category]);

    }
 public function addPost(
     Request $request){
     $this->validate($request,[
         'title'=>'required',
         'subtitle' => 'required',
         'slug' => 'required',
         'body' => 'required',
     ]);
     $post = new post;
     //   $post->image = $imageName;
     $post->title = $request->title;
     $post->subtitle = $request->subtitle;
     $post->slug = $request->slug;
     $post->body = $request->body;
     $post->status = $request->status;
     $post->save();
     return redirect(route('/'));
 }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

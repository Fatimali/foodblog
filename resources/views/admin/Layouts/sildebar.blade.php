<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Food Blog<sup>2</sup></div>
    </a>

    <!-- Divider -->

    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('post.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Post</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('category.index') }}">
            <i class="fa fa-circle-o"></i>
            <span>Categories</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('tag.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Tags</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('user.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>User</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('role.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Roles</span></a>
    </li>
    <hr class="sidebar-divider">
    <li class="nav-item ">
        <a class="nav-link" href="{{ route('permission.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>Permissions</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">




</ul>
<!-- End of Sidebar -->

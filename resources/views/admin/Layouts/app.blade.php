<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin/layouts/head')
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">
    @include('admin/layouts/sildebar')
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

        @include('admin/layouts/Topbar')
            @section('main-content')
              @show
        @include('admin/layouts/footer')
        </div>
    </div>
</div>

<!-- Page Preloder -->

<!-- Header section -->

<!-- Header section end -->


<!-- Hero section -->


<!-- Footer section  -->


<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->

</body>

</html>
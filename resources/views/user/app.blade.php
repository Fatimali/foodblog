<!DOCTYPE html>
<html lang="en">
<head>
    @include('user/layouts/head')
</head>
<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Header section -->


@section('main-contaent')
    @show
<!-- Hero section -->


<!-- Footer section  -->
<footer class="footer-section set-bg" data-setbg="img/footer-bg.jpg">
    @include('user/layouts/footer')
</footer>
<!-- Footer section end -->



<!--====== Javascripts & Jquery ======-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>
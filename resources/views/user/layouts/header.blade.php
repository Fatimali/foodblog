<header class="header-section">


    <div class="header-top">
        <div class="container">
            @yield('menu')
            <a href="index.html" class="site-logo">
                <img src="img/logo.png" alt="">
            </a>
            <div class="nav-switch">
                <i class="fa fa-adn"></i>
            </div>
            <div class="header-search">
                <a href="#"><i class="fa fa-search"></i></a>
            </div>
            <ul class="main-menu">
                <li><a href="index.html">Home</a></li>
                <li><a href="about.html">Features</a></li>
                <li><a href="recipes.html">Receipies</a></li>
                <li><a href="#">Reviews</a></li>
                <li><a href="contact.html">Contact</a></li>
            </ul>
        </div>
    </div>
</header>
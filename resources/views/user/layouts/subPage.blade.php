<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
    <div class="container">
        <h2>@yield('title')</h2>
    </div>
</section>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('auth/login', function () {
    return view('auth.login');
});


Route::get('Login', function () {
    return view('user/Login');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/home', function () {
    return view('admin.home');
})->name('post');

//Admin Routes
Route::group(['namespace' => 'Admin'],function(){
    Route::get('admin/home','HomeController@index')->name('admin.home');
    // Users Route
    Route::resource('admin/user','UserController');
    // Roles Routes
    Route::resource('admin/role','RoleController');
    // Permission Routes
    Route::resource('admin/permission','PermissionController');
    // Post Routes
    Route::resource('admin/post','PostContraller');
    // Tag Routes
    Route::resource('admin/tag','TagContraller');
    // Category Routes
    Route::resource('admin/category','CategoryContraller');
    // Admin Auth Routes
    Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin-login', 'Auth\LoginController@login');
});

//User Routes
Route::group(['namespace' => 'User'],function(){
    Route::get('/','HomeContraller@index');
    Route::get('post/{post}','PostController@post')->name('post');
    Route::get('post/tag/{tag}','HomeController@tag')->name('tag');
    Route::get('post/category/{category}','HomeController@category')->name('category');
    //vue routes
    Route::post('getPosts','PostController@getAllPosts');
    Route::post('saveLike','PostController@saveLike');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
